"""
Exercise 1: What is the function of the secondary memory in a com-
puter? 
Answer 1: answer is c, long term storage

Exercise 2: What is a program?
Answer 2: A set of instructions that a computer can run


Exercise 3: What is the difference between a compiler and an inter-
preter?
Answer 3: They both translate the high-level code into machine code, but where the compiler reads the whole program, the interpreter takes it line-by-line
Often interpreters are faster when analysing code, but compilers really shine once you get a program compiled once.

Exercise 4: Which of the following contains “machine code”?
Answer 4: 

Exercise 5: What is wrong with the following code:
Answer 5: the "print" function is misspelled.

Exercise 6: Where in the computer is a variable such as “x” stored after
the following Python line finishes?
x = 123
Answer 6: c) 


Exercise 7: What will the following program print out:
x = 43
x = x + 1
print(x)
Answer 7: b)44

Exercise 8: Explain each of the following using an example of a human capability:
(1) Central processing unit >>> 
(2) Main Memory >>> Short-term memory or maybe thoughts?
(3) Secondary Memory >>> Long-term memory
(4) Input Device >>> Senses
(5) Output Device >>> voice box? anything that enables communication really, could even be that part of the brain

Exercise 9: How do you fix a “Syntax Error”?
Syntax errors are from misspellings often, or missing characters. Check your work and see if you are missing a character.
"""