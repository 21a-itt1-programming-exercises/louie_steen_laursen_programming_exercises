width = 17
height = 12.0
print("Expression 1 is " + str(width/2))
print("And expression 1's type is " + str(type(width/2)))
print("Expression 2 is " + str(width/2.0))
print("And expression 2's type is " + str(type(width/2.0)))
print("Expression 3 is " + str(height/3))
print("And expression 3's type is " + str(type(height/3)))
print("Expression 4 is " + str(1+2*5))
print("And expression 4's type is " + str(type(1+2*5)))