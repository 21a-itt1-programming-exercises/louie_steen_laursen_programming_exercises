"""Main file, handles user input and display of GameState object"""

import pygame
import Engine

width = height = 768
dimension = 8
square_size = width // dimension
max_fps = 15
assets = {}


def load_images():
    pieces = ["wR","wN","wB","wQ","wK","wP","bR","bN","bB","bQ","bK","bP"]
    for piece in pieces:
        assets[piece] = pygame.transform.scale(pygame.image.load("Assets/" + piece + ".png"),(square_size,square_size))

def main():
    pygame.init()
    screen = pygame.display.set_mode((width, height))
    clock = pygame.time.Clock()
    screen.fill(pygame.Color("white"))
    gs = Engine.GameState()
    valid_moves = gs.get_valid_moves()
    move_was_made = False
    load_images()
    running = True
    selected_square = ()
    clicks = []
    while running:  
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.KEYDOWN: 
                if e.key == pygame.K_z:
                    gs.undo_move()
                    move_was_made = True
                    gs.get_valid_moves() 
            elif e.type == pygame.MOUSEBUTTONDOWN:
                mousexy = pygame.mouse.get_pos()
                y = mousexy[0] // square_size    
                x = mousexy[1] // square_size
                if selected_square == (x, y):
                    selected_square = ()
                    clicks = []
                else:
                    selected_square = (x, y)
                    clicks.append(selected_square)
                if len(clicks) == 2:
                    #print(clicks[0] + clicks[1]) used to check if mouse coordinates are handled correctly
                    #print(move.chess_notation()) used to check if chess_notation is handling the mouse coordinates correctly
                    move = Engine.Move(clicks[0],clicks[1],gs.board)
                    if move in valid_moves:
                        gs.make_move(move)
                        move_was_made = True
                        
                    #very important to clear both these v
                    selected_square = ()
                    clicks = [] 
            if move_was_made == True:
                valid_moves = gs.get_valid_moves() #resets the valid moves after a move
                move_was_made = False
                   
        draw_game_state(gs, screen)
        clock.tick(max_fps)
        pygame.display.flip()


def draw_squares(screen):
    colors =[pygame.Color("white"),pygame.Color("gray20")]
    for y in range(dimension):
        for x in range(dimension):
            color = colors[((y + x)%2)]
            pygame.draw.rect(screen, color, pygame.Rect(y*square_size, x*square_size,square_size,square_size)) 


def draw_pieces(board, screen):
    for y in range(dimension):
        for x in range(dimension):
            piece = board[y][x]
            if piece != "__":
                screen.blit(assets[piece], pygame.Rect(x*square_size,y*square_size,square_size,square_size))



def draw_game_state(gs, screen):
    draw_squares(screen)
    draw_pieces(gs.board,screen)


main()
  