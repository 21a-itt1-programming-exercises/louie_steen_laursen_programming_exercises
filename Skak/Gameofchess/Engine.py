"""Game controller, controls the the gamestate and the valid moves."""


from pygame.constants import TIMER_RESOLUTION


class GameState():
    def __init__(self):
        self.board = [
            ["bR","bN","bB","bQ","bK","bB","bN","bR"],
            ["bP","bP","bP","bP","bP","bP","bP","bP"],
            ["__","__","__","__","__","__","__","__"],
            ["__","__","__","__","__","__","__","__"],
            ["__","__","__","__","__","__","__","__"],
            ["__","__","__","__","__","__","__","__"],
            ["wP","wP","wP","wP","wP","wP","wP","wP"],
            ["wR","wN","wB","wQ","wK","wB","wN","wR"]
            ]
        self.whiteTurn = True
        self.moveLog = []
    def make_move(self, move):
        if self.board[move.SRCx][move.SRCy] != "__":
            self.board[move.SRCx][move.SRCy] = "__"
            self.board[move.DSTx][move.DSTy] = move.moved_piece
            self.moveLog.append(move)
            self.whiteTurn = not self.whiteTurn
    def undo_move(self):
        if len(self.moveLog) != 0: 
            move = self.moveLog.pop()
            self.board[move.SRCx][move.SRCy] = move.moved_piece
            self.board[move.DSTx][move.DSTy] = move.finished_move
            self.whiteTurn = not self.whiteTurn
# checking for checks is bothersome
# the following is my attempt at the pseudo code that is needed for making sure you dont checkmate yourself.
# get all possible moves for the player
# for each possible moves, check the validity by doing the following:
# make the actual move in question
# generate all possible moves for the opposing player, assuming boardstate is after move in question
# check if any of these moves from the opponent checks the king
# if previous is false, the move is valid and should be added
# now a list of only the valid moves can be returned. Valid as in you cannot check your king. 

    def get_valid_moves(self):
        return self.get_all_moves()
    def get_all_moves(self):
        moves = []
        for r in range(len(self.board)):
            for c in range(len(self.board[r])):
                piece_color = self.board[r][c][0]
                if (piece_color == "w" and self.whiteTurn) or (piece_color == "b" and not self.whiteTurn):
                    piece = self.board[r][c][1]
                    if piece == "P":
                        self.get_pawn_moves(r,c,moves)
                    elif piece == "R":
                        self.get_rook_moves(r,c,moves)
                        
                    elif piece == "B":
                        self.get_bishop_moves(r,c,moves)
                        
        return moves


    def get_pawn_moves(self, r, c, moves):
        
        if self.whiteTurn == True:
            if self.board[r-1][c] == "__": 
                moves.append(Move((r,c),(r-1, c),self.board))
                if r == 6 and self.board[r-2][c] == "__":
                    moves.append(Move((r,c),(r-2, c),self.board))
            if c-1 >= 0: 
                if self.board[r-1][c-1][0] == "b":
                    moves.append(Move((r,c),(r-1,c-1),self.board))
            if c+1 <= 7:
                if self.board[r-1][c+1][0] == "b":
                    moves.append(Move((r,c),(r-1,c+1),self.board))
        else:
            if self.board[r+1][c] == "__": 
                moves.append(Move((r,c),(r+1, c),self.board))
                if r == 1 and self.board[r+2][c] == "__":
                    moves.append(Move((r,c),(r+2, c),self.board))
            if c-1 >= 0: 
                if self.board[r+1][c-1][0] == "w":
                    moves.append(Move((r,c),(r+1,c-1),self.board))
            if c+1 <= 7:
                if self.board[r+1][c+1][0] == "w":
                    moves.append(Move((r,c),(r+1,c+1),self.board))
            
        



    def get_rook_moves(self, r, c, moves):
        dirs = ((-1,0),(0,-1),(1,0),(0,1))
        opponentColor = "b" if self.whiteTurn else "w"
        for x in dirs:
            for i in range(1,8):
                final_row = r + x[0] * i
                final_col = r + x[1] * i
                if 0 <= final_row < 8 and 0 <= final_col < 8:
                    final_piece = self.board[final_row][final_col]
                    if final_piece == "__":
                        print(final_row,final_col) 
                        moves.append(Move((r,c),(final_row,final_col),self.board))
                    elif final_piece[0] == opponentColor:
                        moves.append(Move((r,c),(final_row,final_col), self.board))
                        break
                    else:
                        break
                else:
                    break    

    def get_bishop_moves(self, r, c, moves):
        dirs = ((-1,-1),(1,-1),(-1,1),(1,1))
        opponentColor = "b" if self.whiteTurn else "w"
        for x in dirs:
            for i in range(1,8):
                final_row = r + x[0] * i
                final_col = r + x[1] * i
                if 0 <= final_row < 8 and 0 <= final_col < 8:
                    final_piece = self.board[final_row][final_col]
                    if final_piece == "__": 
                        moves.append(Move((r,c),(final_row,final_col),self.board))
                    elif final_piece[0] == opponentColor:
                        moves.append(Move((r,c),(final_row,final_col), self.board))
                        break
                    else:
                        break
                else:
                    break  
                



class Move():
    rank_to_row = {"1":7,"2":6,"3":5,"4":4,"5":3,"6":2,"7":1,"8":0}
    row_to_rank = {v:k for k, v in rank_to_row.items()}
    file_to_column = {"a":0,"b":1,"c":2,"d":3,"e":4,"f":5,"g":6,"h":7}
    column_to_file = {v:k for k, v in file_to_column.items()}

    def __init__(self,SRC,DST,board):
        self.SRCx = SRC[0]
        self.SRCy = SRC[1]
        self.DSTx = DST[0]
        self.DSTy = DST[1]
        self.moved_piece = board[self.SRCx][self.SRCy]
        self.finished_move = board[self.DSTx][self.DSTy]
        self.numerical_move = self.SRCx * 1000 + self.SRCy * 100 + self.DSTx *10 +self.DSTy


    def __eq__(self, other):
        if isinstance(other, Move):
            return self.numerical_move == other.numerical_move
        return False

    def chess_notation(self):
        return self.get_rank_and_file(self.SRCx,self.SRCy) + self.get_rank_and_file(self.DSTx, self.DSTy)
        
    def get_rank_and_file(self,row,column):
        return self.row_to_rank[row] + self.column_to_file[column]
